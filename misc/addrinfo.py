#! /usr/bin/env python

import argparse
import socket

def parse_args() ->argparse.Namespace:
    parser = argparse.ArgumentParser(description="Get addrinfo")
    parser.add_argument('-H', '--host', dest='host', help='host name')
    parser.add_argument('-p', '--port', dest='port', help='port name')
    parser.add_argument('-a', '--address-family', dest='family', default=0,
    help='address family')
    parser.add_argument('-t', '--type', dest='type', default=0,
    help='socket type')
    parser.add_argument('-f', '--flags', dest='flags', default=0,
    help='flags')
    args = parser.parse_args()
    return args

args = parse_args()
print(socket.getaddrinfo(args.host, args.port, family=args.family,
type=args.type, flags=args.flags))
