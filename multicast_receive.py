#! /usr/bin/env python

import argparse
import socket
import struct
import quopri
import base64
from typing import Callable

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Multicast receiver")
    parser.add_argument(
        '-e', '--encoding', dest='encoding',
        choices=['plain', 'quoted', 'base16', 'base64'],
        default='quoted', help='encoding (default: quoted)')
    parser.add_argument(
        '-f', '--address-family', dest='af', choices=['ipv4', 'ipv6'],
        default='ipv4', help='address family (default: ipv4)')
    parser.add_argument(
        '-i', '--interface',  dest='interface', help='interface name')
    parser.add_argument('address', help='multicast address')
    parser.add_argument('port', help='port')
    return parser.parse_args()

def bind_ipv4_to_receive(args: argparse.Namespace) -> socket.socket:
    family = socket.AF_INET
    af, socktype, proto, canonname, sa = socket.getaddrinfo(
        '0.0.0.0', args.port, family=family, type=socket.SOCK_DGRAM)[0]
    s = socket.socket(af, socktype, proto)
    s.bind(sa) 
    multicast_addr = socket.inet_pton(family, args.address)
    if args.interface is None: interface = '0.0.0.0'
    else: interface = args.interface
    local_addr = socket.inet_pton(family, interface)
    multicast = struct.pack('4s4s', multicast_addr, local_addr)
    s.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, multicast)
    return s

def bind_ipv6_to_receive(args: argparse.Namespace) -> socket.socket:
    family = socket.AF_INET6
    af, socktype, proto, canonname, sa = socket.getaddrinfo(
        '::', args.port, family=family, type=socket.SOCK_DGRAM)[0]
    s = socket.socket(af, socktype, proto)
    s.bind(sa)
    if args.interface is not None:
        interface = socket.if_nametoindex(args.interface)
        # https://linuxjm.osdn.jp/html/LDP_man-pages/man7/ipv6.7.html
        s.setsockopt(
            socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_IF, interface)
    return s

def bind_to_receive(args: argparse.Namespace) -> socket.socket:
    if args.af == 'ipv4': return  bind_ipv4_to_receive(args)
    elif args.af == 'ipv6': return bind_ipv6_to_receive(args)
    assert 0

def pass_through(bin: bytes) -> bytes: return bin

def select_encoder(encoding: str) -> Callable[[bytes], bytes]:
    if encoding == 'plain': return pass_through
    elif encoding == 'quoted': return quopri.encodestring
    elif encoding == 'base16': return base64.b16encode
    elif encoding == 'base64': return base64.b64encode
    assert 0

def encode_bytes(bin: bytes,encoder: Callable[[bytes], bytes]) -> str:
    encoded = encoder(bin)
    return encoded.decode()

def receive_str(reader, encoder: Callable[[bytes], bytes]):
    received = reader.recv(2048)
    return encode_bytes(received, encoder)

def receive_all(reader, encoder: Callable[[bytes], bytes]):
    while True:
        str = receive_str(reader, encoder)
        print(str)

args = parse_args()
encoder = select_encoder(args.encoding)
with bind_to_receive(args) as reader:
    receive_all(reader, encoder)
