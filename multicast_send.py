#! /usr/bin/env python

import argparse
import fileinput
import socket
import quopri
import base64
from typing import Callable

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Multicast sender")
    parser.add_argument(
        '-e', '--encoding', dest='encoding',
        choices=['plain', 'quoted', 'base16', 'base64'],
        default='quoted', help='encoding (default: quoted)')
    parser.add_argument(
        '-f', '--address-family', dest='af', choices=['ipv4', 'ipv6'],
        default='ipv4', help='address family (default: ipv4)')
    parser.add_argument(
        '-i', '--interface', dest='interface', help='interface name')
    parser.add_argument(
        '-t', '--ttl', dest='ttl', type=int, choices=range(-1, 256),
        default= -1, metavar='-1..255', help='TTL (default: -1)')
    parser.add_argument('address', help='multicast address')
    parser.add_argument('port', help='port')
    parser.add_argument('file', nargs='*', help='files')
    return parser.parse_args()

def connect_ipv4_to_send(args: argparse.Namespace) -> socket.socket:
    family = socket.AF_INET
    af, socktype, proto, canonname, sa = socket.getaddrinfo(
        args.address, args.port, type=socket.SOCK_DGRAM,
        family=family)[0]
    # https://docs.python.org/ja/3/library/socket.html#example
    s = socket.socket(af, socktype, proto)
    if args.interface is not None:
        src_addr_list = socket.getaddrinfo(
            args.interface, None, type=socket.SOCK_DGRAM, family=family)
        s_af, s_socktype, s_proto, s_canonname, s_sa = src_addr_list[0]
        src = socket.inet_pton(family, s_sa[0])
        # https://www.geekpage.jp/programming/winsock/multicast.php
        s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, src)
    s.connect(sa)
    ttl = args.ttl if args.ttl >= 0 else 1
    s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
    return s

def connect_ipv6_to_send(args: argparse.Namespace) -> socket.socket:
    family = socket.AF_INET6
    af, socktype, proto, canonname, sa = socket.getaddrinfo(
        args.address, args.port, type=socket.SOCK_DGRAM,
        family=family)[0]
    # https://docs.python.org/ja/3/library/socket.html#example
    s = socket.socket(af, socktype, proto)
    if args.interface is not None:
        interface = socket.if_nametoindex(args.interface)
        # https://linuxjm.osdn.jp/html/LDP_man-pages/man7/ipv6.7.html
        s.setsockopt(
            socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_IF, interface)
    s.connect(sa)
    s.setsockopt(
        socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_HOPS, args.ttl)
    return s

def connect_to_send(args: argparse.Namespace) -> socket.socket:
    if args.af == 'ipv4': return connect_ipv4_to_send(args)
    elif args.af == 'ipv6': return connect_ipv6_to_send(args)
    assert 0

def pass_through(bin: bytes) -> bytes: return bin

def select_decoder(encoding: str) -> Callable[[bytes], bytes]:
    if encoding == 'plain': return pass_through
    elif encoding == 'quoted': return quopri.decodestring
    elif encoding == 'base16': return base64.b16decode
    elif encoding == 'base64': return base64.b64decode
    assert 0

def decode_line(line: str, decoder: Callable[[bytes], bytes]) -> bytes:
    str = line.rstrip()
    return decoder(str.encode())

def send_line(to, decoder: Callable[[bytes], bytes], line):
    bin = decode_line(line, decoder)
    to.sendall(bin)

def send_all(to, decoder: Callable[[bytes], bytes], files):
    for line in fileinput.input(files):
        send_line(to, decoder, line)

args = parse_args()
decoder = select_decoder(args.encoding)
with connect_to_send(args) as writer:
    send_all(writer, decoder, args.file)
