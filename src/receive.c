/**
 * @file receive.c
 * @author Mitsutoshi Nakano (ItSANgo@gmail.com)
 * @brief Multicast receiver
 * @version 0.1
 * @date 2022-02-27
 * @copyright Copyright (c) 2022 Mitsutoshi Nakano
 */

#include <assert.h>
#include <sysexits.h>
#include <err.h>
#include <getopt.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * @brief Print usage, and end.
 * @param e exit status.
 */
void
usage(int e)
{
	fprintf(e ? stderr : stdout,
	"usage: multicast_receive [-s source_address] address port\n");
	exit(e);
}

/**
 * @brief Arguments.
 */
struct args {
	char *multicast;
	char *src;
	char *port;
};

/**
 * @brief Parse args.
 * @param argc argc from main.
 * @param argv argv from main.
 * @return struct args arguments data.
 * @see https://linux.die.net/man/3/getopt_long
 */
struct args
parse_args(int argc, char *argv[])
{
	struct args arg = { NULL };
	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"source", required_argument, 0, 's' },
			{ 0, 0, 0, 0 }
		};
		int c = getopt_long(argc, argv, "s:", long_options,
			&option_index);
		if (c == -1) {
			break;
		}
		switch (c) {
		case 's':
			arg.src = optarg;
			break;
		case '?':
			usage(EX_USAGE);
			break;
		default:
			assert(0);
			abort();
			break;
		}
	}
	if (optind + 1 > argc) {
		usage(EX_USAGE);
	}
	arg.multicast = argv[optind];
	arg.port = argv[optind + 1];
	return arg;
}

/** @brief the socket descriptor.  */
static int sock = -2;

/**
 * @brief Close the socket.
 */
void close_sock(void)
{
	if (close(sock)) {
		warn("close(%d)", sock);
	}
}

/**
 * @brief Create a mreq object.
 * @param mreq a mreq object.
 * @param arg arguments data.
 * @param family address family.
 * @see
 *   https://linuxjm.osdn.jp/html/LDP_man-pages/man3/inet_addr.3.html
 */
void
create_mreq(struct ip_mreq *mreq, const struct args *arg, int family)
{
	if (arg->src) {
		if (inet_pton(family, arg->src,  &mreq->imr_interface)
		!= 1) {
			err(EX_CANTCREAT, "inet_ptopn(%d, %s, %p)",
			family, arg->src, &mreq->imr_interface);
		}
	} else {
		mreq->imr_interface.s_addr = INADDR_ANY;
	}
	if (inet_pton(family, arg->multicast, &mreq->imr_multiaddr) != 1) {
		err(EX_CANTCREAT, "inet_ptopn(%d, %s, %p)", family,
		arg->multicast, &mreq->imr_multiaddr);
	}
}

/**
 * @brief Prepare a socket.
 * @param arg arguments.
 * @return int a socket descriptor.
 * @see
 *   https://www.geekpage.jp/programming/winsock/multicast.php
 *   https://linuxjm.osdn.jp/html/LDP_man-pages/man7/ip.7.html
 *   https://man7.org/linux/man-pages/man3/getaddrinfo.3.html
 */
int
prepare_socket(const struct args *arg)
{
	struct addrinfo hint = { 0 };
	hint.ai_family = AF_INET;
	hint.ai_socktype = SOCK_DGRAM;
	struct addrinfo *any;
	int gai_status;
	if ((gai_status = getaddrinfo("0.0.0.0", arg->port, &hint, &any))
	!= 0) {
		err(EX_UNAVAILABLE, "getaddrinfo: %d:%s", gai_status,
			gai_strerror(gai_status));
	}
	struct ip_mreq mreq = { 0 };
	create_mreq(&mreq, arg, any->ai_family);
	int fd;
	if ((fd = socket(any->ai_family, any->ai_socktype,
	any->ai_protocol)) == -1) {
		err(EX_OSERR, "socket(%d, %d, %d)", any->ai_family,
		any->ai_socktype, any->ai_protocol);
	}
	sock = fd;
	atexit(close_sock);
	if (bind(fd, any->ai_addr, any->ai_addrlen)) {
		err(EX_CANTCREAT, "bind(%d, %p, %llu)", fd,
		(void *)any->ai_addr, (unsigned long long)any->ai_addrlen);
	}
	// setsockoptは、bind以降で行う必要あり.
	if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
	&mreq, sizeof(mreq)) != 0) {
		err(EX_NOPERM, "setsockopt(%d, %d, %d, %p{%s, %s}, %llu)",
		fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *)&mreq, 
		arg->multicast, (arg->src) ? arg->src : "ANY",
		(unsigned long long)sizeof mreq);
	}
	freeaddrinfo(any);
	any = NULL;
	return fd;
}

/**
 * @brief Read all packets.
 * @param fd a socket descriptor.
 */
void
read_all(int fd)
{
	for (char buf[2048] = { 0 }; ; memset(buf, 0, sizeof buf)) {
		ssize_t len = recv(fd, buf, sizeof(buf), 0);
		if (len == -1) {
			err(EX_IOERR, "recv(%d, %s, %llu, 0)", fd, buf,
			(unsigned long long)sizeof buf);
		}
		if (len == 0) {
			break;
		}
		if (fwrite(buf, len, 1, stdout) != 1) {
			warn("fwrite(%s, %lld, %p)", buf,
			(unsigned long long)len, stdout);
		}
	}
}

/**
 * @brief Main.
 * @param argc the number of arguments.
 * @param argv arguments.
 * @return int exit status. 
 * @see 
 *   https://nxmnpg.lemoda.net/ja/3/sysexits
 */
int
main(int argc, char *argv[])
{
	struct args arg = parse_args(argc, argv);
	int fd = prepare_socket(&arg);
	read_all(fd);
	exit(EX_OK);
}
